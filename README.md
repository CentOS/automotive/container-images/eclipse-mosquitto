## Eclipse Mosquitto

This is the Containerfile for the [eclipse
mosquitto](https://github.com/eclipse/mosquitto) project.

### Build

```bash
podman build -f Containerfile -t mos:latest
```

### Run

1. Start the `mosquitto` server _by default will listen on port 1883_

```bash
podman run --rm --name mos_daemon --pod new:mos mos:latest
```

2. Start subcriber and create a topic:

```bash
podman run \
    --rm --name mos_sub \
    --pod mos \
    --entrypoint mosquitto_sub \
    mos:latest -t 'demo/topic' -v
```

3. Monitor the pod to check that everything is working:

```bash
podman pod logs -f mos
```

4. Publish a message in the topic:

```bash
podman run \
    --rm --name mos_pub \
    --pod mos \
    --entrypoint mosquitto_pub \
    mos:latest -t 'demo/topic' -m 'hello world!'
```

### Testing multiple messages

```bash
for i in {1..10};do
    podman run \
        --rm --name mos_pub"${i}" \
        --pod mos \
        --entrypoint mosquitto_pub \
        mos:latest -t 'demo/topic' -m "Message ${i}"
done
```

### Adding custom settings  

Custom configurationg are supported, those can be enable when mounted in the
directory `/etc/mosquitto/mosquitto.d/`.  
e.g:   

```bash
podman run \
    --rm --name mos_daemon \
    --pod mos \
    -v $(pwd)/mycustom.conf:/etc/mosquitto/mosquitto.d/mycustom.conf:Z \
    mos:latest
```

for more about configurations follow [this doc](https://mosquitto.org/man/mosquitto-conf-5.html)
